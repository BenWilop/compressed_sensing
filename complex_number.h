#ifndef _COMPLEX_NUMBER_H
#define _COMPLEX_NUMBER_H

#include <iostream>

class Sparse_Matrix;
class Vector;

class Complex_Number { // complex number
    private:
        double* cn;    // complex numbers is saved as 2-vector of doubles

        static void cnError(const char str[]);  // error message

    public:
        explicit Complex_Number(double a = 0, double b = 0);  // constructor, Re(z) = a, Im(z) = b
        Complex_Number(const Complex_Number&);          // copy constructor

        double& operator()(size_t);       // standard access to values, Re(z) = z(0), Im(z) = z(1)
        double operator()(size_t) const;  // read access to const 

        // assigments
        Complex_Number& operator=(const Complex_Number&);   
        Complex_Number& operator=(double);   
        Complex_Number& operator+=(const Complex_Number&);  
        Complex_Number& operator+=(double);
        Complex_Number& operator-=(const Complex_Number&);
        Complex_Number& operator-=(double);
        Complex_Number& operator*=(const Complex_Number&);
        Complex_Number& operator*=(double);
        Complex_Number& operator/=(const Complex_Number&);
        Complex_Number& operator/=(double);

        // special functions
        double abs() const;    // absolute value
        Complex_Number sgn() const;     // sign
        Complex_Number conj() const;     // complex conjugate
        
        // arithmetic operations with complex numbers
        friend Complex_Number operator+(const Complex_Number&, const Complex_Number&); 
        friend Complex_Number operator-(const Complex_Number&, const Complex_Number&);  
        friend Complex_Number operator-(const Complex_Number&);                 
        friend Complex_Number operator*(const Complex_Number&, const Complex_Number&);
        friend Complex_Number operator/(const Complex_Number&, const Complex_Number&);

        // arithmetic operations with real numbers
        friend Complex_Number operator+(double, const Complex_Number&);
        friend Complex_Number operator+(const Complex_Number&, double);
        friend Complex_Number operator-(double, const Complex_Number&);
        friend Complex_Number operator-(const Complex_Number&, double);
        friend Complex_Number operator*(double, const Complex_Number&);
        friend Complex_Number operator*(const Complex_Number&, double);
        friend Complex_Number operator/(double, const Complex_Number&);
        friend Complex_Number operator/(const Complex_Number&, double);

        // comparisons with complex numbers
        friend bool operator==(const Complex_Number&, const Complex_Number&);
        friend bool operator!=(const Complex_Number&, const Complex_Number&);

        // comparisons with real numbers
        friend bool operator==(double, const Complex_Number&);
        friend bool operator==(const Complex_Number&, double);
        friend bool operator!=(double, const Complex_Number&);
        friend bool operator!=(const Complex_Number&, double);
        
        // output
        friend std::ostream& operator<<(std::ostream&, const Complex_Number&);
};

#endif

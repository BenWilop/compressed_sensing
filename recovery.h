#ifndef _RECOVERY_H  
#define _RECOVERY_H

#include <iostream>
#include <unordered_map>
#include <set>
#include <vector>
#include "complex_number.h"
#include "vector.h"
#include "sparse_matrix.h"


// recovery algorithms
// measurement matrix A, measurement vector y, sparsity level s

//optimizaton algorithms
// Vector basis_pursuit(Sparse_Matrix A, Vector b); // basis pursuit
// Vector quadratically_constrained_basis_pursuit(Sparse_Matrix A, Vector b); // quadratically constrained basis pursuit
// Vector basis_pursuit_denoising(Sparse_Matrix A, Vector b); // basis pursuit denoising
// Vector lasso(Sparse_Matrix A, Vector b); // LASSO
// Vector dantzig_selector(Sparse_Matrix A, Vector b); // dantzig selector

// greedy methods
Vector orthogonal_matching_pursuit(const Sparse_Matrix &A, const Vector &b, double eps = 0.0001, int k_max = 100); // orthogonal matching pursuit
Vector compressive_sampling_matching_pursuit(const Sparse_Matrix &A, const Vector &b, int s, double eps = 0.0001, int k_max = 100); // compressive sampling matching pursuit
Vector subspace_pursuit(const Sparse_Matrix &A, const Vector &b, int s, double eps = 0.0001, int k_max = 2); // subspace pursuit

// thresholding-based methods
Vector basic_thresholding(const Sparse_Matrix &A, const Vector &b, int s); // basic thresholding
Vector iterative_hard_thresholding(const Sparse_Matrix &A, const Vector &b, int s, double eps = 0.0001, int k_max = 100); // iterative hard thresholding
Vector hard_thresholding_pursuit(const Sparse_Matrix &A, const Vector &b, int s, double eps = 0.0001, int k_max = 100); // hard thresholding pursuit

#endif

#include <iostream>
#include <unordered_map>
#include <cmath> 

#include "complex_number.h"
#include "vector.h"
#include "sparse_matrix.h"
#include "recovery.h"



int main()
{
    Complex_Number z(1,0);
    Complex_Number w(0,0);
    Sparse_Matrix A(3,3);
    A.put(0,0,z); A.put(0,1,z); A.put(0,2,w);
    A.put(1,0,w); A.put(1,1,z); A.put(1,2,w);
    A.put(2,0,w); A.put(2,1,w); A.put(2,2,z);
    Vector v(3);
    //v(0) = 2;
    v(1) = 1;
    std::cout << A << "\n";
    std::cout << A.orth_proj(v) << "\n";

    std::set<int> supp;
    supp.insert(1);
    std::cout << "min || A.s(supp)*x - y || for x supp-sparse: \n"; 
    std::cout << A.orth_proj_sparse(v, supp) << "\n";
    std::cout << "basic_thresholding: \n";
    std::cout << basic_thresholding(A, v, 1) << "\n";
    std::cout << "iterative_hard_thresholding: \n";
    std::cout << iterative_hard_thresholding(A, v, 1) << "\n";
    std::cout << "hard_thresholding_pursuit: \n";
    std::cout << hard_thresholding_pursuit(A, v, 1) << "\n";
    std::cout << "orthogonal_matching_pursuit: \n";
    std::cout << orthogonal_matching_pursuit(A, v, 1) << "\n";
    std::cout << "compressive_sampling_matching_pursuit: \n";
    std::cout << compressive_sampling_matching_pursuit(A, v, 1) << "\n";
    std::cout << "subspace_pursuit: \n";
    std::cout << subspace_pursuit(A, v, 1) << "\n";

    /*Sparse_Matrix B(3,3);
    B.put(0,0,z); B.put(0,1,w); B.put(0,2,w);
    B.put(1,0,z); B.put(1,1,z); B.put(1,2,w);
    B.put(2,0,w); B.put(2,1,w); B.put(2,2,z); 
    Vector u(3);
    u(0) = 3;
    u(1) = 4;
    std::cout << u << "\n";
    std::cout << B << "\n";
    std::cout << B.gaussian_elem(u) << "\n";*/
}   
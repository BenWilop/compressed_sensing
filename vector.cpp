#include "vector.h"

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <set>
#include <vector>
#include <algorithm>

// =======================
//      constructors
// =======================

// ----- constructor -----

Vector::Vector(size_t l) {
#ifndef NDEBUG
    if (l <= 0) vecError("number of elements needs to be positive!");
#endif

    length = l;

    vec = new Complex_Number[l];
    if (vec == NULL) vecError("not enough memory!");

    for (size_t i = 0; i < l; i++) (*this)(i) = 0;
}

// ----- copy constructor -----

Vector::Vector(const Vector &x) {
    length = x.length;

    vec = new Complex_Number[length];
    if (vec == NULL) vecError("not enough memory!");

    for (size_t i = 0; i < length; i++) (*this)(i) = x(i);
}

// ===========================================
//      write and read values
// ===========================================

// ----- standard access to values -----

Complex_Number &Vector::operator()(size_t i) {
#ifndef NDEBUG
    if (i >= length) vecError("invalid index!");
#endif

    return vec[i];
}

// ----- read access to const -----

Complex_Number Vector::operator()(size_t i) const {
#ifndef NDEBUG
    if (i >= length) vecError("invalid index!");
#endif

    return vec[i];
}

// =====================
//      assigments
// =====================

// ----- assigment "=" ----

Vector &Vector::operator=(const Vector &x) {
    if (length != x.length) {
        delete[] vec;
        length = x.getLength();
        vec = new (std::nothrow) Complex_Number[length];
        if (vec == NULL) vecError("not enough memory!");
    }

    for (size_t i = 0; i < length; i++) (*this)(i) = x(i);

    return *this;
}

// ----- assignment with addition "+=" ----

Vector &Vector::operator+=(const Vector &x) {
#ifndef NDEBUG
    if (this->getLength() != x.getLength()) {
        Vector::vecError("incopatible dimension for 'vector += vector'!");
    }
#endif
    for(size_t i=0; i < this->getLength(); i++) {
        this->operator()(i) += x(i);
    }

    return *this;
}

// ----- assignment with subtraction "-=" ----

Vector &Vector::operator-=(const Vector &x) {
#ifndef NDEBUG
    if (this->getLength() != x.getLength()) {
        Vector::vecError("incopatible dimension for 'vector -= vector'!");
    }
#endif

    return *this += -x;
}

// ----- assignment with multiplication "*=" ----

Vector &Vector::operator*=(Complex_Number c) {
    for(size_t i=0; i < this->getLength(); i++) {
        this->operator()(i) *= c;
    }

    return *this;
}

// ----- assignment with division "/=" ----

Vector &Vector::operator/=(Complex_Number c) {
#ifndef NDEBUG
    if (c == 0) {
        Vector::vecError("division with 0!");
    }
#endif

    return *this *= 1/c;
}

// ==============================
//      new dimension
// ==============================

// ----- new dimension -----

Vector &Vector::redim(size_t l) {
    vec = new Complex_Number[l];
    length = l;
    return *this;
}

// ==============================
//      special functions
// ==============================

// ----- conjugate -----

Vector Vector::conj() const{
    Vector v = *this;
    for(size_t i=0; i < this->getLength(); i++) {
        v(i) = v(i).conj();
    }
    return v;
}

// ----- support -----

std::set<int> Vector::supp() const{
    std::set<int> supp;
    for(size_t i=0; i < this->getLength(); i++) {
        if(this->operator()(i) != 0) {
            supp.insert(i);
        }   
    }
    return supp;
}

// ----- restriction of v on S -----

Vector Vector::s(const std::set<int> supp) const{
    Vector v = *this;
    for(size_t i=0; i < this->getLength(); i++) {
        if(supp.count(i) == 0) {
            v(i) = 0;
        }   
    }
    return v;
}

// ----- hard thresholding operator index -----

std::set<int> Vector::l_s(int s) const{
    std::vector< std::pair <int,int> > indx_vect; // (abs(v(i), i)
    for(size_t i=0; i < this->getLength(); i++) {
        indx_vect.push_back(std::make_pair((this->operator()(i)).abs(), i));
    }
    std::sort(indx_vect.begin(), indx_vect.end(), std::greater<>());

    std::set<int> supp;
    int n = this->getLength();
    for(int i = 0; i < std::min(s, n); i++) {
        supp.insert(indx_vect[i].second);
    }
    return supp;
}

// ----- hard thresholding operator -----

Vector Vector::h_s(int s) const{
    Vector v = *this;
    v = v.s(v.l_s(s));
    return v;
}

// ----- sparse embedding -----

Vector Vector::sparse_embedding(int n, std::set<int> S) const{
#ifndef NDEBUG
    if (!(!S.empty() && this->getLength() <= S.size() && (int) S.size() <= n && 0 <= *S.begin() && *S.end() < n)) {
        Vector::vecError("incopatible dimension for embedding!");
    }
#endif    
    
    Vector x(n);
    int k = 0;
    for (auto elem: S) {
        x(elem) = this->operator()(k);
    }
    return x;
}


// ======================
//      vector norms
// ======================

// ----- p norm (p) -----

double Vector::normp(int p) const {
    double norm = 0;
    for(size_t i=0; i < this->getLength(); i++) {
        Complex_Number z = this->operator()(i);
        norm += pow(z.abs(), p);
    }
    return pow(norm, (double) 1/p);
}

// ----- 1 norm (1) -----

double Vector::norm1() const {
    return this->normp(1);
}

// ----- euclidian norm (2) -----

double Vector::norm2() const {
    return this->normp(2);
}

// ----- maximal norm (inf) -----

double Vector::normMax() const {
  double norm = 0;
  for(size_t i=0; i < this->getLength(); i++) {
      Complex_Number z = this->operator()(i);
      norm = fmax(norm, z.abs());
  }
  return norm;
}

// ============================================
//      arithmetic operations with vectors
// ============================================

// ----- addition "+" -----

Vector operator+(const Vector &x, const Vector &y) {
#ifndef NDEBUG
    if (x.getLength() != y.getLength()) {
        Vector::vecError("incopatible dimension for 'vector + vector''!");
    }
#endif

    Vector z = x;
    return z += y;
}

// ----- subtraction "-" -----

Vector operator-(const Vector &x, const Vector &y) {
#ifndef NDEBUG
    if (x.getLength() != y.getLength()) {
        Vector::vecError("incopatible dimension for 'vector - vector'!");
    }
#endif

    Vector z = x;
    return z -= y;
}

// ----- change sign "-" -----

Vector operator-(const Vector &x) {
    Vector z = x;

    for(size_t i=0; i < z.getLength(); i++) {
        z(i) = -x(i);
    }

    return z;
}

// ----- Division Vector/Vector "/"  <-->  D^(-1)*x -----

// implementation komponentenweise
Vector operator/(const Vector &x, const Vector &d) {
#ifndef NDEBUG
    if (x.getLength() != d.getLength()) {
        Vector::vecError("incopatible dimension for 'vector / vector'!");
    }
#endif
    Vector z = x;
    for(size_t i=0; i < x.getLength(); i++) {
        z(i) /= d(i);
    }
    return z;
}

// ----- scalarproduct "*" -----

Complex_Number operator*(const Vector &x, const Vector &y) {
#ifndef NDEBUG
    if (x.getLength() != y.getLength()) {
        Vector::vecError("incopatible dimension for 'vector * vector'!");
    }
#endif

    Complex_Number skalar(0,0);
    for(size_t i=0; i < x.getLength(); i++) {
        skalar += x(i) * y(i);
    }
    return skalar;
}

// ============================================
//      arithmetic operations with scalars
// ============================================

// ----- multiplication scalar*vector "*" -----

Vector operator*(Complex_Number c, const Vector &x) {
    Vector z = x;
    return z *= c;
}

// ----- multiplication vector*scalar "*" -----

Vector operator*(const Vector &x, Complex_Number c) {
    return c * x;
}

// ----- division vector/scalar "/" -----

Vector operator/(const Vector &x, Complex_Number c) {
#ifndef NDEBUG
    if (c == 0) {
        Vector::vecError("division with 0!");
    }
#endif
    return 1/c * x;
}

// ==============================
//      comparisons
// ==============================

// ----- equality "==" -----

bool operator==(const Vector &x, const Vector &y) {
    if (x.length != y.length) return false;

    for (size_t i = 0; i < x.length; i++)
        if (x(i) != y(i)) return false;

    return true;
}

// ----- inequality "!=" -----

bool operator!=(const Vector &x, const Vector &y) {
    return !(x == y);
}

// ==========================
//      output
// ==========================

// ----- output "<<" -----

std::ostream &operator<<(std::ostream &s, const Vector &x) {
    s << std::setiosflags(std::ios::right);
    for (size_t i = 0; i < x.length; i++) s << "(" << std::setw(4) << i << ") " << x(i);
    
    return s << std::endl;
}

// ==========================
//      errors
// ==========================

// ----- error message "str" -----

void Vector::vecError(const char str[]) {
    std::cerr << "\nVector error: " << str << '\n' << std::endl;
    std::abort();
}

#include "recovery.h"

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <set>
#include <vector>


// =============================
//      greedy methods
// =============================

// ----- orthogonal matching pursuit -----

Vector orthogonal_matching_pursuit(const Sparse_Matrix &A, const Vector &y, double eps, int k_max) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    int k = 0;
    Vector x_k(A.getRows()); // x_0 = 0
    std::set<int> S_k; // S_0 = empty set
    while ((A*x_k-y).norm2() > eps && k <= k_max && k <= (int) A.getRows()) { // stopping criterium ||Ax-y|| < eps or k > k_max
        k++;
        // argmax {(|A*(y-Ax))_j| | j}
        std::set<int> j_max = (A.H()*(y-A*x_k)).l_s(1); // 1 element set
        S_k.insert(j_max.begin(), j_max.end());
        x_k = A.orth_proj_sparse(y, S_k);
    }

    return x_k;
}

// ----- compressive sampling matching pursuit -----

Vector compressive_sampling_matching_pursuit(const Sparse_Matrix &A, const Vector &y, int s, double eps, int k_max) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    int k = 0;
    Vector x_k(A.getRows()); // x_0 = 0
    Vector u_k(A.getRows()); // x_0 = 0
    while ((A*x_k-y).norm2() > eps && k <= k_max) { // stopping criterium ||Ax-y|| < eps or k > k_max
        k++;
        std::set<int> U_k = x_k.l_s(2*s);
        std::set<int> supp_x_n = x_k.supp();
        U_k.insert(supp_x_n.begin(), supp_x_n.end());
        u_k = A.orth_proj_sparse(y, U_k);
        x_k = u_k.h_s(s);
    }

    return x_k;
}

// ----- compressive sampling matching pursuit -----

Vector subspace_pursuit(const Sparse_Matrix &A, const Vector &y, int s, double eps, int k_max) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    int k = 0;
    Vector x_k(A.getRows()); // x_0 = 0
    std::set<int> S_k; // S_0 = empty set
    while ((A*x_k-y).norm2() > eps && k <= k_max) { // stopping criterium ||Ax-y|| < eps or k > k_max
        k++;
        std::set<int> U_k = S_k;
        std::set<int> l_s_temp = (A.H()*(y-A*x_k)).l_s(s);
        U_k.insert(l_s_temp.begin(), l_s_temp.end());
        Vector u_n = A.orth_proj_sparse(y, U_k);
        S_k = u_n.l_s(s);
        x_k = A.orth_proj_sparse(y, S_k);
    }

    return x_k;
}

// ==============================================
//      thresholding-based methods
// ==============================================

// ----- basic thresholding -----

Vector basic_thresholding(const Sparse_Matrix &A, const Vector &y, int s) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    Vector z = A.H()*y;
    std::set<int> S = z.l_s(s);
    return A.orth_proj_sparse(y, S);
}

// ----- basic thresholding -----

Vector iterative_hard_thresholding(const Sparse_Matrix &A, const Vector &y, int s, double eps, int k_max) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    int k = 0;
    Vector x_k(A.getRows()); // x_0 = 0
    while ((A*x_k-y).norm2() > eps && k <= k_max) { // stopping criterium ||Ax-y|| < eps or k > k_max
        k++;
        x_k = (x_k + A.H()*(y - A*x_k)).h_s(s);
    }

    return x_k;
}

// ----- hard thresholding pursuit -----

Vector hard_thresholding_pursuit(const Sparse_Matrix &A, const Vector &y, int s, double eps, int k_max) {
#ifndef NDEBUG
    if (A.getRows() != y.getLength()) {
        std::cerr << "incopatible dimension for 'recovery of Ax=y!" << std::endl;
    }
#endif

    int k = 0;
    Vector x_k(A.getRows()); // x_0 = 0
    std::set<int> S_k;
    while ((A*x_k-y).norm2() > eps && k <= k_max) { // stopping criterium ||Ax-y|| < eps or k > k_max
        k++;
        S_k = (x_k + A.H()*(y - A*x_k)).l_s(s);
        x_k = A.orth_proj_sparse(y, S_k);
    }

    return x_k;
}
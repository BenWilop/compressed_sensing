#ifndef _VECTOR_H
#define _VECTOR_H

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
#include "complex_number.h"

class Sparse_Matrix;

class Vector {
    private:
        Complex_Number* vec;    // pointer on field of type Complex_Number for values of Vector
        size_t length;  // vector dimension

        static void vecError(const char str[]);  // error message

    public:
        explicit Vector(size_t i = 1);  // constructor with vector dimension
        Vector(const Vector&);          // copy constructor

        Complex_Number& operator()(size_t);       // standard access to values
        Complex_Number operator()(size_t) const;  // read access to const 

        // assigments
        Vector& operator=(const Vector&);
        Vector& operator+=(const Vector&); 
        Vector& operator-=(const Vector&);
        Vector& operator*=(Complex_Number);
        Vector& operator/=(Complex_Number);

        // new dimension
        Vector& redim(size_t);  // new dimension
        size_t getLength() const { // length
            return length;
        }
        
        // special functions
        Vector conj() const; // complex conjugate
        std::set<int> supp() const; // support
        Vector s(const std::set<int>) const; // restriction of v on S
        std::set<int> l_s(int) const; // hard thresholding operator index
        Vector h_s(int) const; // hard thresholding operator
        Vector sparse_embedding(int n, std::set<int> S) const; // embedding of |S| sparse vector in C^n with support S

        // vector norms
        double normp(int) const;  // p norm (p)
        double norm1() const;    // 1 norm (1)                    
        double norm2() const;    // euclidian norm (2)
        double normMax() const;  // maximal norm (inf)

        // arithmetic operations with vectors
        friend Vector operator+(const Vector&, const Vector&);  
        friend Vector operator-(const Vector&, const Vector&);
        friend Vector operator-(const Vector&);
        friend Vector operator/(const Vector&, const Vector&); // D^(-1)*x

        friend Complex_Number operator*(const Vector&, const Vector&);  // scalarproduct
        
        // arithmetic operations with scalars
        friend Vector operator*(Complex_Number, const Vector&);         
        friend Vector operator*(const Vector&, Complex_Number);
        friend Vector operator/(const Vector&, Complex_Number);

        // comparisons
        friend bool operator==(const Vector&, const Vector&);  
        friend bool operator!=(const Vector&, const Vector&);

        // output
        friend std::ostream& operator<<(std::ostream&, const Vector&);  
};

#endif

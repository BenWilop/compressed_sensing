#include "sparse_matrix.h"

#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <set>
#include <vector>

// =======================
//      constructors
// =======================

// ----- constructor -----

Sparse_Matrix::Sparse_Matrix(size_t r, size_t c) {
#ifndef NDEBUG
    if (r <= 0 || c <= 0) {
        matError("row and column numbers need to be positive!");
    }
#endif
    rows = r;
    cols = c;
}

// ----- copy constructor -----

Sparse_Matrix::Sparse_Matrix(const Sparse_Matrix &x) {
    rows = x.rows;
    cols = x.cols;

    for (auto it_x: x.mat) {
        mat.insert(std::make_pair(it_x.first, it_x.second));
    }
}

// ===========================================
//      write and read values
// ===========================================

// ----- read access to values -----

Complex_Number Sparse_Matrix::operator()(size_t r, size_t c) const {
#ifndef NDEBUG
    if(r >= rows || c >= cols) matError("invalid index!");
#endif

    coord_t coord = std::make_pair(r, c);
    auto iterator = mat.find(coord);
    if (iterator != mat.end()) {
        return iterator->second;
    }
    Complex_Number z(0,0);
    return z;
}

// ----- write access to values -----

void Sparse_Matrix::put(size_t i, size_t j, Complex_Number x){
#ifndef NDEBUG
    if(i >= rows || j >= cols) matError("invalid index!");
#endif
    coord_t coord = std::make_pair(i, j);
    auto it = mat.find(coord);
    if( it != mat.end() ) {
        it->second = x;
    } else {
        mat.insert(std::make_pair(coord, x));
    }
}

Complex_Number Sparse_Matrix::get(size_t r, size_t c) const {
    return this->operator()(r,c);
}


// =====================
//      assigments
// =====================

// ----- assigment "=" ----

Sparse_Matrix &Sparse_Matrix::operator=(const Sparse_Matrix &x) {
#ifndef NDEBUG
    if (x.getRows() != getRows() || x.getCols() != getCols()) {
        matError("incopatible dimension for 'matrix = matrix'!");
    }
#endif

    mat.clear();
    for (auto it_x: x.mat) {
        mat.insert(std::make_pair(it_x.first, it_x.second));
    }
    return *this;
}


// ----- assignment with addition "+=" ----

Sparse_Matrix &Sparse_Matrix::operator+=(const Sparse_Matrix &x) {
#ifndef NDEBUG
    if (this->getRows() != x.getRows() || this->getCols() != x.getCols()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix += matrix'!");
    }
#endif
    for (auto it_x: x.mat) {
        auto it_mat = mat.find(it_x.first);
        if (it_mat == mat.end()) {
            mat.insert(std::make_pair(it_x.first, it_x.second));
        } else {
            it_mat->second += it_x.second;
        }
    }
    return *this;
}

// ----- assignment with subtraction "-=" ----

Sparse_Matrix &Sparse_Matrix::operator-=(const Sparse_Matrix &x) {
#ifndef NDEBUG
    if (this->getRows() != x.getRows() || this->getCols() != x.getCols()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix -= matrix'!");
    }
#endif
    for (auto it_x: x.mat) {
        auto it_mat = mat.find(it_x.first);
        if (it_mat == mat.end()) {
            mat.insert(std::make_pair(it_x.first, -it_x.second));
        } else {
            it_mat->second -= it_x.second;
        }
    }
    return *this;
}

// ----- assignment with multiplication "*=" ----

Sparse_Matrix &Sparse_Matrix::operator*=(const Sparse_Matrix &x) {
#ifndef NDEBUG
    if (this->getCols() != x.getRows()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix *= matrix'!");
    }
#endif
    cols = x.getCols();
    hashmap_t mat_prod;
    for (auto it_x: x.mat) {
        for (auto it_this: mat) {
            if (it_this.first.second == it_x.first.first) {       
                Complex_Number val = it_x.second * it_this.second;
                coord_t coord = std::make_pair(it_this.first.first, it_x.first.second);
                auto it = mat_prod.find(coord);
                if( it != mat_prod.end() ) {
                    it->second += val;
                } else {
                    mat_prod.insert(std::make_pair(coord, val));
                }
            }
        }
    }
    this->mat = mat_prod;
    return *this;
}

// ----- assignment with multiplication scalar "*=" ----

Sparse_Matrix &Sparse_Matrix::operator*=(Complex_Number c) {
    for (auto it_mat: mat) {
        auto it_temp = mat.find(it_mat.first);
        it_temp->second *= c;   
    }
    return *this;
}

// ----- assignment with division scalar "*=" ----

Sparse_Matrix &Sparse_Matrix::operator/=(Complex_Number c) {
#ifndef NDEBUG
    if (c == 0) {
        Sparse_Matrix::matError("division with 0!");
    }
#endif

    return *this *= 1/c;
}


// ==============================
//      new dimension
// ==============================

// ----- new dimension -----

Sparse_Matrix& Sparse_Matrix::redim(size_t m, size_t n) {
    rows = m;
    cols = n;
    mat.clear();
    return *this;
}

// ==============================
//      special functions
// ==============================

// ----- conjugate -----

Sparse_Matrix Sparse_Matrix::conj() const{
    Sparse_Matrix x = *this;
    for (auto it_x: x.mat) { 
        auto it_temp = x.mat.find(it_x.first);  
        it_temp->second = (it_temp->second).conj();
    }
    return x;
}

// ----- transpose -----

Sparse_Matrix Sparse_Matrix::T() const{
    Sparse_Matrix x(cols, rows);
    for (auto it_x: mat) { 
        x.mat.insert(std::make_pair(std::make_pair(it_x.first.second, it_x.first.first), it_x.second));
    }
    return x;
}

// ----- conjugate transpose (hermitian transpose) -----

Sparse_Matrix Sparse_Matrix::H() const{
    return (this->conj()).T();
}

// ----- restriction of A on S -----

Sparse_Matrix Sparse_Matrix::s(std::set<int> supp) const{
    // submatrix sorted by col index
    Sparse_Matrix A(rows, supp.size());
    std::vector<int> supp_vec(supp.begin(), supp.end());
    std::sort(supp_vec.begin(), supp_vec.end());
    for (int j = 0; j < (int) supp.size(); j++) { // columns
        for (int i = 0; i < (int) rows; i++) { // rows
            //std::cout << "i: " << i << ", j:" << j << ", supp_vec[j]: " << supp_vec[j] << "\n";
            //std::cout << (this->operator()(i, supp_vec[j])) << "\n";
            A.put(i, j, this->operator()(i, supp_vec[j]));
        }
    }
    return A;
}

// ==============================
//      row operations
// ==============================

// ----- swap rows -----

void Sparse_Matrix::swap_rows(size_t i, size_t j){
#ifndef NDEBUG
    if(i >= rows || j >= cols) matError("invalid index!");
#endif
    for(int k = 0; k < (int) cols; k++){
        Complex_Number temp = this->operator()(i,k);
        this->put(i, k, this->operator()(j,k));
        this->put(j, k, temp);
    }
}

// ----- multiply row -----

void Sparse_Matrix::mult_row(size_t i, Complex_Number z){
#ifndef NDEBUG
    if(i >= rows) matError("invalid index!");
#endif
    for(int k = 0; k < (int) cols; k++){
        this->put(i, k, z*this->operator()(i,k));
    }
}

// ----- add rows -----

void Sparse_Matrix::add_rows(size_t i, size_t j, Complex_Number z){ // adds j*z on i
#ifndef NDEBUG
    if(i >= rows || j >= cols) matError("invalid index!");
#endif
    for(int k = 0; k < (int) cols; k++){
        this->put(i, k, this->operator()(i,k) + z*this->operator()(j,k));
    }
}

// ==============================
//      linear solvers
// ==============================

// ----- gaussian elimination -----
// TODO: SWAPPING ROWS CHANGES RESULT!!!
Vector Sparse_Matrix::gaussian_elem(Vector y){
    // assumes existence of x and solves for one solution of Ax = y where free variables are all 0
#ifndef NDEBUG
    if (rows != y.getLength()) {
        Sparse_Matrix::matError("incopatible dimension for linear system of equations!");
    }
#endif
    Sparse_Matrix A = *this;
    //std::cout << this->getCols();
    //std::cout << "A.getCols() " << A.getCols() << "\n";
    //std::cout << "A " << A << "\n";
    // forward elimination
    int zero_count = 0;
    for (int k = 0; k < (int) rows; k++) {
        //std::cout << "k: " << k << "\n";
        // max pivot
        int max_pivot = k - zero_count;
        Complex_Number max_pivot_val = A(k-zero_count,k);
        for (int l = (k-zero_count) + 1; l < (int) rows; l++) {
            Complex_Number new_pivot = A(l,k);
            if (new_pivot.abs() > max_pivot_val.abs()) {
                max_pivot = l;
                max_pivot_val = new_pivot;
            }
        }
       // std::cout << "max_pivot: " << max_pivot << "\n";
        if (max_pivot_val.abs() == 0) {
            zero_count += 1;
            continue;
        }

        if (max_pivot != k-zero_count) {
            A.swap_rows(k-zero_count, max_pivot);
            Complex_Number temp = y(k-zero_count);
            y(k-zero_count) = y(max_pivot);
            y(max_pivot) = temp;
        }
        
        // elimination
        for (int l = (k-zero_count) + 1; l < (int) rows; l++) {
            //std::cout << "l: " << l << ", k: " << k << "\n";
            //std::cout << "y(l): " << y(l) << ", y(k) * (- A(l,k) / max_pivot_val): " << y(k) * (- A(l,k) / max_pivot_val) << "\n";
            y(l) += y(k) * (- A(l,k) / max_pivot_val);
            A.add_rows(l, k-zero_count, - A(l,k) / max_pivot_val);
        }
        //std::cout << "y: " << y << "\n";
    }
    //std::cout << "A: " << A << "\n";
    //std::cout << "y: " << y << "\n";    
    // back substitution
    Vector x(A.getCols()); // solution vector
    for (int k = A.getRows() - 1; k >= 0; k--) {

        // find pivot element
        int pivot_col = 0;
        while (pivot_col < (int) A.getCols() && A(k, pivot_col) == 0) {
            pivot_col += 1;
        }
        if (pivot_col == (int) A.getCols()) {
            if (y(k) != 0) {
                Sparse_Matrix::matError("linear system of equations not solvable!");
                return x;
            } else {
                continue;
            }
        }

        // solve equation for x(k)
        //std::cout << "k: " << k << "\n";
        //std::cout << "pivot_col: " << pivot_col << "\n";
        x(pivot_col) = y(k);
        for (int l = pivot_col + 1; l < (int) A.getCols() - 1; l++) {
            //std::cout << "A(k,l)" << A(k,l) << "\n";
            x(pivot_col) -= x(l)*A(k,l);
        }
        //std::cout << "A(k,pivot_col)" << A(k,pivot_col) << "\n";
        x(pivot_col) /= A(k,pivot_col);
    }
    return x;
}

// ----- orthogonal projection -----

Vector Sparse_Matrix::orth_proj(Vector y) const{
    // min ||Ax-y||
    // x = (A^H*A)^(-1)*A^H y
#ifndef NDEBUG
    if (rows != y.getLength()) {
        Sparse_Matrix::matError("incopatible dimension for linear system of equations!");
    }
#endif
    // (A^H*A) x = A^H y
    Sparse_Matrix A = *this;
    Sparse_Matrix B = A.H()*A;
    Vector x = B.gaussian_elem(A.H()*y);
    return x;
}

// ----- sparse orthogonal projection -----

Vector Sparse_Matrix::orth_proj_sparse(Vector y, std::set<int> supp) const{
    // min ||Ax-y|| s.t. x is supp-sparse
    Sparse_Matrix A_s = this->s(supp);
    //return A_s.orth_proj(y);
    Vector x_s = A_s.orth_proj(y);
    return x_s.sparse_embedding(this->getRows(), supp);
}

// ==============================================
//      arithmetic operations with matrices
// ==============================================

// ----- addition "+" -----

Sparse_Matrix operator+(const Sparse_Matrix &x, const Sparse_Matrix &y) {
#ifndef NDEBUG
    if (x.getRows() != y.getRows() || x.getCols() != y.getCols()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix + matrix!");
    }
#endif
    Sparse_Matrix z = x;
    return z += y;
}

// ----- subtraction "-" -----

Sparse_Matrix operator-(const Sparse_Matrix &x, const Sparse_Matrix &y) {
#ifndef NDEBUG
    if (x.getRows() != y.getRows() || x.getCols() != y.getCols()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix - matrix!");
    }
#endif
    Sparse_Matrix z = x;
    return z -= y;
}

// ----- change sign "-" -----

Sparse_Matrix operator-(const Sparse_Matrix &x) {
    Sparse_Matrix z = x;
    Complex_Number w(-1,0);
    return z *= w;
}

// ----- multiplication "*" -----

Sparse_Matrix operator*(const Sparse_Matrix &x, const Sparse_Matrix &y) {
#ifndef NDEBUG
    if (x.getCols() != y.getRows()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix * matrix!");
    }
#endif
    Sparse_Matrix z = x;
    return z *= y;
}

// ==============================================
//      arithmetic operations with scalars
// ==============================================

// ----- multiplication scalar*matrix "*" -----

Sparse_Matrix operator*(Complex_Number c, const Sparse_Matrix &x) {
    Sparse_Matrix z = x;
    return z *= c;
}

// ----- multiplication matrix*scalar "*" -----

Sparse_Matrix operator*(const Sparse_Matrix &x, Complex_Number c) {
    Sparse_Matrix z = x;
    return z *= c;
}

// ----- division matrix/scalar "/" -----

Sparse_Matrix operator/(const Sparse_Matrix &x, Complex_Number c) {
    Sparse_Matrix z = x;
    return z /= c;
}

// =============================================
//      arithmetic operations with vectors
// =============================================

// ----- multiplication matrix*vector "*" -----
Vector operator*(const Sparse_Matrix &x, const Vector &y){
#ifndef NDEBUG
    if (x.getCols() != y.getLength()) {
        Sparse_Matrix::matError("incopatible dimension for 'matrix * vector!");
    }
#endif
    Vector z(x.getRows());
    for (auto it_x: x.mat) {
        z(it_x.first.first) += it_x.second*y(it_x.first.second);
    }
    return z;
 }

// ----- multiplication (vector*matrix)^T "*" -----
Vector operator*(const Vector &y, const Sparse_Matrix &x){
#ifndef NDEBUG
    if (x.getRows() != y.getLength()) {
        Sparse_Matrix::matError("incopatible dimension for 'vector * matrix!");
    }
#endif
    Vector z(x.getCols());
    for (auto it_x: x.mat) {
        z(it_x.first.second) += it_x.second*y(it_x.first.first);
    }
    return z;
 }

// ==============================
//      comparisons
// ==============================

// ----- equality "==" -----

bool operator==(const Sparse_Matrix &x, const Sparse_Matrix &y) {
    if (x.getRows() != y.getRows() || x.getCols() != y.getCols()) {
        return false;    
    } else {
    //x in y
    for (auto it_x: x.mat) {
        auto it_y = y.mat.find(it_x.first);
        if (it_y == y.mat.end() || it_x.second != it_y->second) {
            return false;
        }
    }
    //y in x
    for (auto it_y: y.mat) {
        auto it_x = x.mat.find(it_y.first);
        if (it_x == x.mat.end() || it_y.second != it_x->second) {
            return false;
        }
    }
    return true;
    }
}

// ----- inequality "!=" -----

bool operator!=(const Sparse_Matrix &x, const Sparse_Matrix &y) {
    return !(x == y);
}

// ==========================
//      output
// ==========================

// ----- output "<<" -----

std::ostream &operator<<(std::ostream &s, const Sparse_Matrix &x) {
    s << std::setiosflags(std::ios::right);
    for (auto it_x: x.mat) {
        s << "(" << std::setw(4) << it_x.first.first << ", "<< it_x.first.second << ") " << it_x.second;
    }
    return s << std::endl;
}

// ==========================
//      errors
// ==========================

// ----- error message "str" -----

void Sparse_Matrix::matError(const char str[]) {
    std::cerr << "\nMatrix error: " << str << '\n' << std::endl;
    std::abort();
}

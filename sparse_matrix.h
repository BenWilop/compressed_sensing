#ifndef _SPARSE_MATRIX_H  
#define _SPARSE_MATRIX_H

#include <iostream>
#include <unordered_map>
#include <set>
#include <vector>
#include "vector.h"
#include "complex_number.h"

class Vector;

using coord_t = std::pair<size_t, size_t>;  // datatype of key

struct coord_hash {  // hashfunction for key
    std::size_t operator()(coord_t key) const {
        std::hash<size_t> size_t_hash;
        return size_t_hash(key.first) + size_t_hash(key.second);
    }
};

using hashmap_t = std::unordered_map<coord_t, Complex_Number, coord_hash>;  // hashmap

class Sparse_Matrix {
    private:
        size_t rows, cols;  // matrix dimension
        hashmap_t mat;      // hashmap for matrix elements

        static void matError(const char str[]);  // error message

    public:
        explicit Sparse_Matrix(size_t r = 1, size_t c = 1);  // constructor with matrix dimension
        Sparse_Matrix(const Sparse_Matrix&);                 // copy constructor

        void put(size_t i, size_t j, Complex_Number x);   // write matrix elements
        Complex_Number operator()(size_t i, size_t j) const;  // read matrix elements
        Complex_Number get(size_t i, size_t j) const;
        
        // assigments
        Sparse_Matrix& operator=(const Sparse_Matrix&);   
        Sparse_Matrix& operator+=(const Sparse_Matrix&);  
        Sparse_Matrix& operator-=(const Sparse_Matrix&);
        Sparse_Matrix& operator*=(const Sparse_Matrix&);
        Sparse_Matrix& operator*=(Complex_Number);
        Sparse_Matrix& operator/=(Complex_Number);

        // new dimension
        Sparse_Matrix& redim(size_t, size_t);  // new dimension
        size_t getRows() const { // rows
            return rows;
        }  
        size_t getCols() const { // column
            return cols;
        }  

        // special functions
        Sparse_Matrix conj() const; // complex conjugate
        Sparse_Matrix T() const; // transpose
        Sparse_Matrix H() const; // complex transpose
        Sparse_Matrix s(std::set<int>) const; // restriction of A on S
        
        // row operations
        void swap_rows(size_t, size_t);
        void mult_row(size_t, Complex_Number);
        void add_rows(size_t, size_t, Complex_Number); // adds second row * z on first row

        // linear solvers
        Vector gaussian_elem(Vector); // linear system of equations
        Vector orth_proj(Vector) const ; // orthogonal projection min ||Ax-y||
        Vector orth_proj_sparse(Vector, std::set<int>) const; // orthogonal projection min ||Ax-y|| s.t. x is S-sparse
        
        // arithmetic operations with matrices
        friend Sparse_Matrix operator+(const Sparse_Matrix&, const Sparse_Matrix&);  
        friend Sparse_Matrix operator-(const Sparse_Matrix&, const Sparse_Matrix&);  
        friend Sparse_Matrix operator-(const Sparse_Matrix&);
        friend Sparse_Matrix operator*(const Sparse_Matrix&, const Sparse_Matrix&);                      

        // arithmetic operations with scalars
        friend Sparse_Matrix operator*(Complex_Number, const Sparse_Matrix&);  
        friend Sparse_Matrix operator*(const Sparse_Matrix&, Complex_Number);
        friend Sparse_Matrix operator/(const Sparse_Matrix&, Complex_Number);

        // arithmetic operations with vectors
        friend Vector operator*(const Sparse_Matrix&, const Vector&);  
        friend Vector operator*(const Vector&, const Sparse_Matrix&);  

        // comparisons
        friend bool operator==(const Sparse_Matrix&, const Sparse_Matrix&);  
        friend bool operator!=(const Sparse_Matrix&, const Sparse_Matrix&);
            
        // output
        friend std::ostream& operator<<(std::ostream&, const Sparse_Matrix&);  
};

#endif

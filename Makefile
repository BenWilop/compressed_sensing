CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra -Wpedantic -O2 -fno-inline -fPIC -fPIE

.PHONY: clean

compressed_sensing: main.o recovery.o sparse_matrix.o vector.o complex_number.o
	$(CXX) $(CXXFLAGS) -o $@ $^

meine_loesung.o: main.cpp sparse_matrix.h vector.h complex_number.h
	$(CXX) $(CXXFLAGS) -c $<

complex_number.o: complex_number.cpp complex_number.h
	$(CXX) $(CXXFLAGS) -c $<

vector.o: vector.cpp vector.h complex_number.h
	$(CXX) $(CXXFLAGS) -c $<

sparse_matrix.o: sparse_matrix.cpp sparse_matrix.h vector.h complex_number.h
	$(CXX) $(CXXFLAGS) -c $<

recovery.o: recovery.cpp sparse_matrix.h vector.h complex_number.h
	$(CXX) $(CXXFLAGS) -c $<




clean:
	rm -f main.o recovery.o sparse_matrix.o vector.o complex_number.o compressed_sensing
#include "complex_number.h"

#include <cmath>
#include <cstdlib>
#include <iomanip>

// =======================
//      constructors
// =======================

// ----- constructor -----

Complex_Number::Complex_Number(double a, double b) {
    cn = new double[2];
    (*this)(0) = a;
    (*this)(1) = b;
}

// ----- copy constructor -----

Complex_Number::Complex_Number(const Complex_Number &x) {
    cn = new double[2];
    (*this)(0) = x(0);
    (*this)(1) = x(1);
}

// ===========================================
//      write and read values
// ===========================================

// ----- standard access to values -----

double &Complex_Number::operator()(size_t i) {
    return cn[i];
}

// ----- read access to const -----

double Complex_Number::operator()(size_t i) const {
    return cn[i];
}

// =====================
//      assignments
// =====================

// ----- assignment complex "=" ----

Complex_Number &Complex_Number::operator=(const Complex_Number &x) {
    (*this)(0) = x(0);
    (*this)(1) = x(1);
    return *this;
}

// ----- assignment real "=" ----

Complex_Number &Complex_Number::operator=(double c) {
    (*this)(0) = c;
    return *this;
}

// ----- assignment with addition complex "+=" ----

Complex_Number &Complex_Number::operator+=(const Complex_Number &x) {
    this->operator()(0) += x(0);
    this->operator()(1) += x(1);
    return *this;
}

// ----- assignment with addition real "+=" ----

Complex_Number &Complex_Number::operator+=(double c) {
    this->operator()(0) += c;
    return *this;
}

// ----- assignment with subtraction complex "-=" ----

Complex_Number &Complex_Number::operator-=(const Complex_Number &x) {
    this->operator()(0) -= x(0);
    this->operator()(1) -= x(1);
    return *this;
}

// ----- assignment with subtraction real "-=" ----

Complex_Number &Complex_Number::operator-=(double c) {
    this->operator()(0) -= c;
    return *this;
}

// ----- assignment with multiplication complex "*=" ----

Complex_Number &Complex_Number::operator*=(const Complex_Number &x) {
    double a = this->operator()(0);
    double b = this->operator()(1);
    this->operator()(0) = a*x(0) - b*x(1);
    this->operator()(1) = a*x(1) + b*x(0);
    return *this; 
}

// ----- assignment with multiplication real "*=" ----

Complex_Number &Complex_Number::operator*=(double c) {
    this->operator()(0) *= c;
    this->operator()(1) *= c;
    return *this;
}

// ----- assignment with division complex "/=" ----

Complex_Number &Complex_Number::operator/=(const Complex_Number &x) {
#ifndef NDEBUG
    if (x(0) == 0 && x(1) == 0) {
        Complex_Number::cnError("division with 0");
    }
#endif
    double a = this->operator()(0);
    double b = this->operator()(1);
    this->operator()(0) = (a*x(0) + b*x(1)) / (pow(x(0), 2) + pow(x(1), 2));
    this->operator()(1) = (- a*x(1) + b*x(0)) / (pow(x(0), 2) + pow(x(1), 2));
    return *this;
}

// ----- assignment with division real "/=" ----

Complex_Number &Complex_Number::operator/=(double c) {
#ifndef NDEBUG
    if (c == 0) {
        Complex_Number::cnError("division with 0");
    }
#endif
    this->operator()(0) /= c;
    this->operator()(1) /= c;
    return *this;
}

// =============================
//      special functions
// =============================

// ----- absolute value -----

double Complex_Number::abs() const {
    double norm = 0;
    norm += pow(this->operator()(0), 2);
    norm += pow(this->operator()(1), 2);
    return sqrt(norm);
}

// ----- sign -----

Complex_Number Complex_Number::sgn() const {
    if ((this->operator()(1) == 0) && (this->operator()(1) == 0)) {
        return Complex_Number(0,0);
    }
    double c = this->abs();
    Complex_Number w = *this;
    return w *= c;
}

// ----- conjugate -----

Complex_Number Complex_Number::conj() const {
    Complex_Number z = *this;
    z(1) = - z(1);
    return z;
}

// =====================================================
//      arithmetic operations with complex numbers
// =====================================================


// ----- addition "+" -----

Complex_Number operator+(const Complex_Number &x, const Complex_Number &y) {
    Complex_Number z = x;
    return z += y;
}

// ----- subtraction "-" -----

Complex_Number operator-(const Complex_Number &x, const Complex_Number &y) {
  Complex_Number z = x;
  return z -= y;
}

// ----- change sign "-" -----

Complex_Number operator-(const Complex_Number &x) {
    Complex_Number z = x;
    z(0) = -x(0);
    z(1) = -x(1);
    return z;
}

// ----- multiplication complex*complex "*" -----

Complex_Number operator*(const Complex_Number &x, const Complex_Number &y) {
    Complex_Number z = x;
    return z *= y;
}

// ----- dvision complex*complex "*" -----

Complex_Number operator/(const Complex_Number &x, const Complex_Number &y) {
    Complex_Number z = x;
    return z /= y;
}

// =====================================================
//      arithmetic operations with real numbers
// =====================================================

// ----- addition real+complex "+" -----

Complex_Number operator+(double c, const Complex_Number &x) {
    Complex_Number z = x;
    z(0) += c;
    return z;
}

// ----- addition complex+real "+" -----

Complex_Number operator+(const Complex_Number &x, double c) {
    Complex_Number z = x;
    z(0) += c;
    return z;
}

// ----- subtraction real-real "+" -----

Complex_Number operator-(double c, const Complex_Number &x) {
    Complex_Number z = x;
    z(0) -= c;
    return z;
}

// ----- subtraction complex-real "-" -----

Complex_Number operator-(const Complex_Number &x, double c) {
    Complex_Number z = x;
    z(0) -= c;
    return z;
}

// ----- multiplication real*complex "*" -----

Complex_Number operator*(double c, const Complex_Number &x) {
    Complex_Number z = x;
    return z *= c;
}

// ----- multiplication complex*real "*" -----

Complex_Number operator*(const Complex_Number &x, double c) {
    Complex_Number z = x;
    return z *= c;
}

// ----- division real/complex "/" -----

Complex_Number operator/(double c, const Complex_Number &x) {
#ifndef NDEBUG
    if (c == 0) {
        Complex_Number::cnError("division with 0");
    }
#endif
    Complex_Number z(c, 0);
    return z/x; 
}

// ----- division complex/real "/" -----

Complex_Number operator/(const Complex_Number &x, double c) {
#ifndef NDEBUG
    if (c == 0) {
        Complex_Number::cnError("division with 0");
    }
#endif
    return 1/c * x; 
}

// ===========================================
//      comparisons with complex numbers
// ===========================================

// ----- equality complex, complex "==" -----

bool operator==(const Complex_Number &x, const Complex_Number &y) {
    return x(0) == y(0) && x(1) == y(1);
}

// ----- inequality complex, complex "!=" -----

bool operator!=(const Complex_Number &x, const Complex_Number &y) {
    return !(x == y);
}

// ===========================================
//      comparisons with real numbers
// ===========================================

// ----- equality real, complex "==" -----

bool operator==(double c, const Complex_Number &x) {
    return x(0) == c && x(1) == 0;
}

// ----- equality complex, real "==" -----

bool operator==(const Complex_Number &x, double c) {
    return x(0) == c && x(1) == 0;
}

// ----- inequality real, complex "!=" -----

bool operator!=(double c, const Complex_Number &x) {
    return !(x == c);
}

// ----- inequality complex, real "!=" -----

bool operator!=(const Complex_Number &x, double c) {
    return !(x == c);
}

// ==========================
//      output
// ==========================

// ----- output "<<" -----

std::ostream &operator<<(std::ostream &s, const Complex_Number &x) {
    s << std::setiosflags(std::ios::right);
    s << x(0) << " + " << x(1) << "i";
    return s << std::endl;
}

// ==========================
//      errors
// ==========================

// ----- error message "str" -----

  void Complex_Number::cnError(const char str[]) {
    std::cerr << "\nComplex_Number error: " << str << '\n' << std::endl;
    std::abort();
}

